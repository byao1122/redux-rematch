# "State management with Rematch" course

Learning Point:

- Redux Rematch
- TypeScript
- React : Container structure component pattern


How to start:
- npm install
- npm run start


You can find complete video course on [Youtube](https://www.youtube.com/playlist?list=PLNG2YBDrzK-w1VSeDpMxdGwkb4L6hDy8Z)
